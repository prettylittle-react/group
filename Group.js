import React from 'react';
import PropTypes from 'prop-types';

import {getModifiers} from 'libs/component';

/**
 * Group
 * @description [Description]
 * @example
  <div id="Group"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Group, {
    	title : 'Example Group'
    }), document.getElementById("Group"));
  </script>
 */
class Group extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'group';
	}

	render() {
		const {children} = this.props;

		return <div className={getModifiers(this.baseClass, [`items-${children.length}`])}>{children}</div>;
	}
}

Group.defaultProps = {
	children: null
};

Group.propTypes = {
	children: PropTypes.node
};

export default Group;
