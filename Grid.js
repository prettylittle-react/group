import React from 'react';
import PropTypes from 'prop-types';

import Group from './Group';

import './Grid.scss';

/**
 * Grid
 * @description [Description]
 * @example
  <div id="Grid"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Grid, {
        title : 'Example Grid'
    }), document.getElementById("Grid"));
  </script>
 */
class Grid extends Group {
	constructor(props) {
		super(props);

		this.baseClass = 'grid';
	}
}

Grid.defaultProps = {
	children: null
};

Grid.propTypes = {
	children: PropTypes.node
};

export default Grid;
